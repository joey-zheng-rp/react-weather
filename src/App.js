import React, { Component } from 'react';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import CityCondition from './components/CityCondition';
import {checkWeatherCondition, checkWeatherforecast} from './getWeatherInfo';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            result: {},
            unit: "c",
        };
    }

    changeUnit(unit){
        this.setState({unit: unit});
    }

    load(city) {
        var cityName = city || "Brisbane";
        var promiseCondition = checkWeatherCondition(cityName);
        var promiseForecast = checkWeatherforecast(cityName);
        var promiseAll = Promise.all([promiseCondition,promiseForecast]);

        // promiseAll.then(function (result) {
        //     var finalResult = {...result[0], ...result[1]} ;
        //     this.setState(() => ({result: finalResult}));
        // }.bind(this)).catch(function (err) {
        //     alert(err);
        // });

        promiseAll.then((result) => {
            var finalResult = {...result[0], ...result[1]} ;
            this.setState(() => ({result: finalResult}));
        }).catch(function (err) {
            alert(err);
        });
    }

    componentWillMount(){
        this.load();
    }

    render(){
        return <div id="wrapper">
            <Header />
            <CityCondition result = {this.state.result}
                           unit={this.state.unit}
                           changeUnit={this.changeUnit.bind(this)}
                           load={this.load.bind(this)}/>
            <Footer />
        </div>;
    }
}

export default App;

