import React, { Component } from 'react';

class Clock extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            time: ""
        };
    }

    tick(){
        this.timer = setInterval(() => {
            let now = new Date();
            this.setState({time: now.toLocaleTimeString("arab",{timeZone: this.props.timeZone})});
        },1000)
    }

    componentDidMount(){
        this.tick();
    }

    componentWillUnmount(){
        clearInterval(this.timer);
    }

    render()
    {
        return <span>{this.state.time}</span>;
    }
}

export default Clock;