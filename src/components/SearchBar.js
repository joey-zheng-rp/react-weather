import React, { Component } from 'react';
import Clock from './clock';

class SearchBar extends React.Component{

    render()
    {
        return <nav>
            <input type='text' id='city' ref={(input) => {
                this.text=input}}/>
            <button onClick={() => {
                this.props.load(this.text.value);
                this.text.value = "";
            }}>Load</button>
            <label name="c">C</label>
            <input type="radio" id="c" name="unit" defaultChecked onChange={() => this.props.changeUnit("c")} />
            <label name="f">F</label>
            <input type="radio" id="f" name="unit" onChange={() => this.props.changeUnit("f")}/>
            <Clock timeZone={this.props.timeZone} />
        </nav>;
    }
}

export default SearchBar;