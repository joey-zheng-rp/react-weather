import React, { Component } from 'react';
import logo from '../weatherLogo.png'
function Header(props) {
    // return DOM structure related to header part
    return <header>
        <figure className="wu"><img src={logo} /></figure>
        <h1>My Weather Channel</h1>
        <h2>JSON data from the Weather Underground</h2>
    </header>;
}

export default Header;