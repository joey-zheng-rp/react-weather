import React, { Component } from 'react';

function Forecast (props) {
    return <div>
        <span>{props.weekday}</span>
        <span> <img src={props.imgPath} /></span>
        <span>{props.unit === "c" ? props.highC : props.highF}</span>
        <span>{props.unit === "c" ? props.lowC : props.lowF}</span>
    </div>;
}

export default Forecast;