import SearchBar from './SearchBar';
import React, { Component } from 'react';
import Forecast from './Forecast';

class CityCondition extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            days: 5
        };
    }

    changeDays(days){
        this.setState({days: days});
    }

    render () {
        return <div>
            <SearchBar load={this.props.load}
                       changeUnit={this.props.changeUnit}
                       timeZone={this.props.result.timeZone}/>
            <main>
                <section id="left">
                    <div id="location">{this.props.result.location}</div>
                    <div id="weather">{this.props.result.weather}</div>
                    <div
                        id="temperature">{this.props.unit === "c" ? this.props.result.temperatureC : this.props.result.temperatureF}</div>
                    <div id="desc">{this.props.result.desc}</div>
                </section>

                <section id="right">
                    <label name="5">5-day forecast</label>
                    <input type="radio" id="5" name="days" defaultChecked onChange={() => this.changeDays(5)}/>
                    <label name="10">9-day forecast</label>
                    <input type="radio" id="10" name="days" onChange={() => this.changeDays(10)}/>
                    {this.props.result.forecast ? this.props.result.forecast.map((day, index) => {
                        if (index + 1 <= this.state.days) return <Forecast key={index} unit={this.props.unit} {...day}/>;
                    }) : null}
                </section>
            </main>
        </div>;
    }
}

export default CityCondition;