import axios from 'axios';

export function checkWeatherCondition(city) {
    return axios.get(`http://api.wunderground.com/api/f029e46fd0232d12/geolookup/conditions/q/Australia/${city}.json`)
                .then(response => {
                   return new Promise((resolve,reject) => {
                       let result = {};
                       if(!response.data.location) reject("No search result");
                       result.timeZone = response.data.location.tz_long;
                       result.location = response.data.current_observation.display_location.full;
                       result.weather = response.data.current_observation.weather;
                       result.temperatureC = response.data.current_observation.temp_c + 'c';
                       result.temperatureF = response.data.current_observation.temp_f + 'f';
                       resolve(result);
                   });
                });
}

export function checkWeatherforecast(city) {

    return axios.get(`http://api.wunderground.com/api/f029e46fd0232d12/geolookup/forecast10day/q/Australia/${city}.json`)
                .then(response => {
                    return new Promise((resolve,reject) => {
                        let result = {};
                        if(!response.data.forecast) reject("No search result");
                        result.desc = response.data.forecast.txt_forecast.forecastday[0].fcttext_metric;
                        result.forecast = [];
                        let forecastData = response.data.forecast.simpleforecast.forecastday;
                        for(let i=1; i < response.data.forecast.simpleforecast.forecastday.length; i++) {
                            let forecast = {};
                            forecast.weekday = forecastData[i].date.weekday;
                            forecast.highC = forecastData[i].high.celsius;
                            forecast.highF = forecastData[i].high.fahrenheit;
                            forecast.lowC = forecastData[i].low.celsius;
                            forecast.lowF = forecastData[i].low.fahrenheit;
                            forecast.imgPath = forecastData[i].icon_url;
                            result.forecast.push(forecast);
                        }
                        resolve(result);
                    });
                });
}






